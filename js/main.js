$(document).ready(function () {
    //initialize swiper when document ready
    var mySwiper = new Swiper ('.swiper-container', {
            loop: true,
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            onSlideChangeEnd: function(swiper){
                var backgroundImage =$(swiper.slides[swiper.activeIndex]).css("background-image");
                $("#header-wrapper-background").css("background-image",backgroundImage);
            }
    });

    var options = {
        format: 'dd.mm.yyyy',
        min: Date.now(),
        today: 'Heute',
        clear: 'Leeren',
        close: 'Schließen',
        labelMonthNext: 'Nächster Monat',
        labelMonthPrev: 'Vorheriger Monat',
        labelMonthSelect: 'Wähle einen Monat',
        labelYearSelect: 'Wähle ein Jahr',
        monthsFull: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthsShort: ['Jan', 'Feb', 'Mrz', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        weekdaysFull: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        weekdaysShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        container: 'body'
    }

    var from_$input = $('#date-arrival').pickadate(options);
    from_picker = from_$input.pickadate('picker')

    var to_$input = $('#date-departure').pickadate(options),
    to_picker = to_$input.pickadate('picker')


    // Check if there’s a “from” or “to” date to start with.
    if ( from_picker.get('value') ) {
      to_picker.set('min', from_picker.get('select'))
    }
    if ( to_picker.get('value') ) {
      from_picker.set('max', to_picker.get('select'))
    }

    // When something is selected, update the “from” and “to” limits.
    from_picker.on('set', function(event) {
      if ( event.select ) {
        to_picker.set('min', from_picker.get('select'))    
      }
      else if ( 'clear' in event ) {
        to_picker.set('min', false)
      }
    })
    to_picker.on('set', function(event) {
      if ( event.select ) {
        from_picker.set('max', to_picker.get('select'))
      }
      else if ( 'clear' in event ) {
        from_picker.set('max', false)
      }
})

});