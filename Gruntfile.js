module.exports = function(grunt) {

  grunt.initConfig({
      includes: {
          dev_html: {
              options: {
                  includeRegexp: /^\s*<\!\-\-\s*include\s+(\S+)\s+\-\->$/,
                  duplicates: true,
                  flatten: false
              },
              files: [{
                  expand: true,
                  cwd: 'templates',
                  src: ['**/*.tmpl'],
                  dest: 'dist',
                  ext: '.html'
              }]
          }
      },
      browserSync: {
          dev: {
              bsFiles: {
                  src : 'dist/**/*.*'
              },
              options: {
                  watchTask: true,
                  server: {
                      baseDir: "dist"
                  }
              }
          }
      },
      concat: {
          dist: {
              src: ['js/jquery/jquery-2.1.4.min.js','js/main.js'],
              dest: 'dist/js/main.js'
          },
      },

      autoprefixer: {
      // prefix the specified file
        single_file: {
          options: {
            // Target-specific options go here.
          },
          src: 'dist/css/main.css',
          dest: 'dist/css/main.css'
        },

      },

      sass: {
          dist: {
              files: {
                  'dist/css/main.css': 'sass/main.scss'
              }
          }
      },
      watch: {

          html: {
              files: 'templates/**/*.tmpl',
              tasks: ['includes'],
                options: {},
          },

          js: {
              files: 'js/**/*.js',
              tasks: ['concat'],
              options: {
              },
          },


          sass: {
              files: 'sass/**/*.*',
              tasks: ['sass'],
              options: {
              },
          },
      },
  });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-includes');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.registerTask('default', ['sass', 'concat', 'includes','browserSync','watch']);

};
